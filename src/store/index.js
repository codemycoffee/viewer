import React from "react";
import globalHook from "use-global-hook";
import { actions } from "./actions";

const initialState = {
    models: [],
    likes: [], 
    page: "models"
}



export const useGlobal = globalHook(React, initialState, actions)
