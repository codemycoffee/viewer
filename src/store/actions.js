import ls from "local-storage";
export const actions = {

    addModels: (store, models, page) => {
        if (page === "models") {
            const newModels = [...store.state.models, ...models]
            ls("models", JSON.stringify(newModels))

            store.setState({
                models: newModels
            })
        } else {
            const newModels = [...store.state.likes, ...models]
            ls("likes", JSON.stringify(newModels))

            store.setState({

                likes: newModels
            })
        }
    },

    setPage: (store, page) => {
        ls("page", page)
        store.setState({
            page
        })
    }
}