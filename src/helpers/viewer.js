const fillFrame = (id) => {
    const newFrame = document.getElementById(id)
    
    if (newFrame.getAttribute("src") === null || newFrame.getAttribute("id") !== id) {
        const client = new window.Sketchfab(newFrame)

        client.init(id, {
            autostart:0,
            success: function onSuccess(api) {
                api.addEventListener('viewerready', function () {

                });
            },
            error: function onError() {
                console.log('Viewer error');
            }
        });
    }
}

export {
    fillFrame
}