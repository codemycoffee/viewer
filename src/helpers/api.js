const api = "https://api.sketchfab.com/v3"
const token = "ffd8625dd7c9458dbf1c930cfe7ab0cc"

const retrieveModels = async (page) => {
    const url = `${api}/me/${page}`

    if (page === 'models') {
        const res = await fetchModels(url)
        return res
    } else {
        const res = await fetchModels(url)
        return res
    }
}

async function fetchModels(url) {
    const response = await fetch(url, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Token ${token}`
        },
    });

    return response.json();
}

async function fetchComments(uid) {
    const url = `${api}/comments?model=${uid}`

    const response = await fetch(url)

    return response.json()
}

function filterNew(oldData, newData) {
    const oldUids = oldData.map(data => data.uid)
    const newModels = newData.filter(data => oldUids.indexOf(data.uid) === -1)

    return newModels
}

export {
    retrieveModels,
    fetchComments,
    filterNew
}

