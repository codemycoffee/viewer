import React from "react";
import { MdStar, MdComment, MdRemoveRedEye } from "react-icons/md";
import { formatCount } from "../../helpers";

const Interactions = ({ sectionStyle, model }) => (
    <ul className={sectionStyle}>
        <li>
            <p>{formatCount(model.viewCount)}</p> <MdRemoveRedEye />
        </li>
        <li>
            <p>{formatCount(model.commentCount)}</p> <MdComment />
        </li>
        <li>
            <p>{formatCount(model.likeCount)}</p> <MdStar />
        </li>
    </ul>
)

export {
    Interactions
}