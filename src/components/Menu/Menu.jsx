import React from "react";
import logo from "../../assets/logo.png";
import { MdDashboard, MdStars } from "react-icons/md";
import "./menu.scss"

const Menu = ({setActive}) => (
    <section id="side-menu">
        <ul>
            <li><img src={logo} alt="logo" /></li>
            <li id="models" data-selected onClick={() => setActive("models")}>
                <MdDashboard size={35} />
            </li>
            <li id="likes" onClick={() => setActive("likes")}>
                <MdStars size={35} />
            </li>
        </ul>
    </section>
)

export {
    Menu
}