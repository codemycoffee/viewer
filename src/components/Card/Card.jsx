import React from "react";
import { Interactions } from "../Interactions/Interactions";
import "./card.scss"

const Card = ({ toggleModal, model }) => (
    <div className="card">
        <section className="card-overlay" onClick={() => toggleModal(model.uid)}></section>
        <iframe title={model.name} id={model.uid}></iframe>
        <Interactions sectionStyle="card-details" model={model} />
    </div>
)


export {
    Card
}