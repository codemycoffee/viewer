import React, { useEffect, useState } from "react";
import {useGlobal} from "../../store";
import { Card } from "../Card/Card";
import { Modal } from "../Modal/Modal";
import { fillFrame } from "../../helpers/viewer";
import "./list.scss";

const List = () => {
    const [globalState] = useGlobal()
    const [modal, setModal] = useState({ isOpen: false, uid: "" })

    const models = globalState[globalState.page]
    
    
    useEffect(() => {
        models.map(model =>
            fillFrame(model.uid)
        )
    })


    const toggleModal = (uid) => {
        setModal({
            isOpen: !modal.isOpen,
            uid
        })
    }

    function getModelData(uid) {
        return models.filter(model => model.uid === uid)[0]
    }


    return (
        <>
            {modal.isOpen && <Modal model={getModelData(modal.uid)} toggleModal={toggleModal} uid={modal.uid} />}
            <div id="list">
                {models.map(model =>
                    <Card key={model.uid} toggleModal={toggleModal} model={model} />
                )}
            </div>
        </>
    )
}

export {
    List
}