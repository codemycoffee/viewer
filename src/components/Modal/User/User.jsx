import React from "react";
import "./user.scss"

const User = ({ avatar, displayName, profileUrl }) => (
    <div className="user">
        <img src={avatar !== undefined ? avatar.images[0].url : ""} alt="avatar" />
        <a href={profileUrl}>{displayName}</a>
    </div>
)

export {
    User
}