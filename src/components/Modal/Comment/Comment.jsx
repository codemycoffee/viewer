import React from "react";
import { User } from "../User/User";
import "./comment.scss"

const Comment = ({ commentData }) => {
    return (
        <div className="comment">
            <User avatar={commentData.user.avatar} displayName={commentData.user.displayName} profileUrl={commentData.user.profileUrl} />
            <p>{commentData.body}</p>
        </div>
    )
}

export {
    Comment
}