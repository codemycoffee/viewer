import React, { useState, useEffect } from "react";
import { fetchComments } from "../../helpers/api";
import { Interactions } from "../Interactions/Interactions";
import { User } from "./User/User";
import { Comment } from "./Comment/Comment";
import "./modal.scss"

const Modal = ({ model, toggleModal, uid }) => {

	const [comments, setComments] = useState([])

	async function loadComments() {
		const { results } = await fetchComments(uid)

		setComments(results)
	}

	useEffect(() => {
		loadComments()
	})

	return (
		<div className="modal" onClick={() => toggleModal(uid)}>
			<div className="column-left">
				<div className="header">
					<div id="title">{model.name}</div>
					<Interactions sectionStyle="stats" model={model} />
				</div>
				<iframe title={model.name} id={uid}></iframe>
			</div>
			<div className="column-right">
				<User avatar={model.user.avatar} displayName={model.user.displayName} profileUrl={model.user.profileUrl} />
				{model.tags !== null &&
					<div className="right-section">
						{model.tags.map(tag => {
							return <div key={tag.name} className="tags">{tag.name}</div>
						})}
					</div>
				}
				<div className="description">
					<h3>Description</h3>
					<p>{model.description}</p>
				</div>
				<div id="comments">
					<h3>{comments.length} Comments</h3>
					{comments.length > 1 &&
						<div>
							{comments.map(comment => <Comment key={comment.uid} commentData={comment} />)}
						</div>

					}
				</div>
			</div>
		</div>
	)
}
export {
	Modal
}