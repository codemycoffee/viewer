import React, { useEffect } from 'react';
import ls from "local-storage";
import { useGlobal } from "./store";
import { retrieveModels, filterNew } from "./helpers/api";
import { Menu } from "./components/Menu/Menu";
import { List } from "./components/List/List";
import './App.scss';


const App = () => {

	const [globalState, globalActions] = useGlobal()
	const savedPage = ls("page")

	async function loadUserModels(page) {

		const storedModels = JSON.parse(ls(page))
		const { results } = await retrieveModels(page)

		let newResults

		if (storedModels !== null && globalState[page].length < 1) {
			newResults = storedModels
		} else {
			newResults = filterNew(globalState[page], results)
		}

		if (newResults !== undefined && newResults.length > 1) {
			globalActions.addModels(newResults, page)
		}
	}

	useEffect(() => {
		if (savedPage !== null) {			
			globalActions.setPage(savedPage)
			setActive(savedPage)
			loadUserModels(globalState.page)
		} else {
			loadUserModels(globalState.page)
		}

	}, [])

	const setActive = (page) => {
		const modelsBtn = document.getElementById('models')
		const likedBtn = document.getElementById('likes')

		if (page === "models") {
			modelsBtn.setAttribute('data-selected', "")
			likedBtn.removeAttribute('data-selected', "")

			changePage(page)
		} else {
			modelsBtn.removeAttribute('data-selected', "")
			likedBtn.setAttribute('data-selected', "")

			changePage(page)
		}
	}

	const changePage = (page) => {				
		if (page !== globalState.page) {			
			globalActions.setPage(page)
			loadUserModels(page)
		}
	}

	return (
		<div className="App">
			<>
				<Menu setActive={setActive} />
				<div className="main-container">
					<List />
				</div>
			</>
		</div>
	);

}

export default App;
